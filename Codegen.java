import java.io.*;

// **********************************************************************
// La classe Codegen contient les constantes et op�rations utiles pour g�n�rer le code
//
// Les constantes sont:
//     Registers: FP, SP, RA, V0, V1, A0, T0, T1
//     Values: TRUE, FALSE
//
// Les op�rations comprennent divers m�thodes "generate" qui impriment du code proprement assembl�:
//     generateWithComment
//     generate
//     generateIndexed
//     generateLabeled
//     genPush
//     genPop
//     genLabel
// et une methode nextLabel pour cr�er et retourner un nouveau label.
// **********************************************************************

public class Codegen {
    // fichier o� le code g�n�r� est �crit.
    public static PrintWriter p = null;    

    // valeurs de true et false
    public static final String TRUE = "1";
    public static final String FALSE = "0";

    // registers
    public static final String FP = "$fp";
    public static final String SP = "$sp";
    public static final String RA = "$ra";
    public static final String V0 = "$v0";
    public static final String V1 = "$v1";
    public static final String A0 = "$a0";
    public static final String T0 = "$t0";
    public static final String T1 = "$t1";


    // pour bien imprimer le code g�n�r�
    private static final int MAXLEN = 4;


    // pour g�n�rer les labels
    private static int currLabel = 0;

    // generate avec commentaires
    public static void generateWithComment(String opcode, String comment,
					   String arg1, String arg2,
					   String arg3) {
	int space = MAXLEN - opcode.length() + 2;
	
	p.print("\t" + opcode);
	if (arg1 != "") {
	    for (int k = 1; k <= space; k++) p.print(" ");
	    p.print(arg1);
	    if (arg2 != "") {
		p.print(", " + arg2);
		if (arg3 != "") p.print(", " + arg3);
	    }
	}
	if (comment != "") p.print("\t\t#" + comment);
	p.println();
	
    }

    public static void generateWithComment(String opcode, String comment,
					   String arg1, String arg2) {
	generateWithComment(opcode, comment, arg1, arg2, "");
    }

    public static void generateWithComment(String opcode, String comment,
					   String arg1) {
	generateWithComment(opcode, comment, arg1, "", "");
    }

    public static void generateWithComment(String opcode, String comment) {
	generateWithComment(opcode, comment, "", "", "");
    }

    // generate
    public static void generate(String opcode, String arg1, String arg2,
				String arg3) {
	int space = MAXLEN - opcode.length() + 2;
	
	p.print("\t" + opcode);
	if (arg1 != "") {
	    for (int k = 1; k <= space; k++) p.print(" ");
	    p.print(arg1);
	    if (arg2 != "") {
		p.print(", " + arg2);
		if (arg3 != "") p.print(", " + arg3);
	    }
	}
	p.println();
    }

    public static void generate(String opcode, String arg1, String arg2) {
	generate(opcode, arg1, arg2, "");
    }

    public static void generate(String opcode, String arg1) {
	generate(opcode, arg1, "", "");
    }

    public static void generate(String opcode) {
	generate(opcode, "", "", "");
    }

    // generate (2 strings, un int)
    public static void generate(String opcode, String arg1, String arg2,
				int arg3) {
	int space = MAXLEN - opcode.length() + 2;
	
	p.print("\t" + opcode);
	for (int k = 1; k <= space; k++) p.print(" ");
	p.println(arg1 + ", " + arg2 + ", " + arg3);
    }
    
    // generate (un string, un int)
    public static void generate(String opcode, String arg1, int arg2) {
	int space = MAXLEN - opcode.length() + 2;
	
	p.print("\t" + opcode);
	for (int k = 1; k <= space; k++) p.print(" ");
	p.println(arg1 + ", " + arg2);
	
    }
    
    // generateIndexed
    public static void generateIndexed(String opcode, String arg1,
				      String arg2, int arg3, String comment)
    {
	int space = MAXLEN - opcode.length() + 2;
	
	p.print("\t" + opcode);
	for (int k = 1; k <= space; k++) p.print(" ");
	p.print(arg1 + ", " + arg3 + "(" + arg2 + ")");
	if (comment != "") p.print("\t#" + comment);
	p.println();
	
    }
    
    public static void generateIndexed(String opcode, String arg1,
				       String arg2, int arg3) {
	generateIndexed(opcode, arg1, arg2, arg3, "");
    }

    // generateLabeled
    public static void generateLabeled(String label, String opcode,
				       String comment, String arg1) {
	int space = MAXLEN - opcode.length() + 2;
	
	p.print(label + ":");
	p.print("\t" + opcode);
	if (arg1 != "") {
	    for (int k = 1; k <= space; k++) p.print(" ");
	    p.print(arg1);
	}
	if (comment != "") p.print("\t# " + comment);
	p.println();
	}

    public static void generateLabeled(String label, String opcode,
				       String comment) {
	generateLabeled(label, opcode, comment, "");
    }

    // genPush
    public static void genPush(String s) {
	generateIndexed("sw", s, SP, 0, "PUSH");
	generate("subu", SP, SP, 4);
    }

    // genPop
    public static void genPop(String s) {
	generateIndexed("lw", s, SP, 4, "POP");
	generate("addu", SP, SP, 4);
    }

    // genLabel
    public static void genLabel(String label, String comment) {
	p.print(label + ":");
	if (comment != "") p.print("\t\t" + "# " + comment);
	p.println();
    }
    
    public static void genLabel(String label) {
	genLabel(label, "");
    }
    
    // Return un label diff�rent a chaque fois:
    //        L0 L1 L2, etc.
    public static String nextLabel() {
	Integer k = new Integer(currLabel++);
	String tmp = ".L" + k;
	return(tmp);
    }
}
