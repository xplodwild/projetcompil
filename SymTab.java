
// La classe SymTab cr�e une table des symboles, qui est une LinkedList de Hashtables, chacune contient (ou pas) des symboles.


import java.io.*;
import java.util.*;
                                               
class SymTab {

    LinkedList myList;

    public SymTab() {
	myList = new LinkedList();
	addHashtab();
    }

    public void insert(Sym sym)
	throws DuplicateException, EmptySymTabException
{
	Hashtable tab;

	if (myList.size() == 0) throw new EmptySymTabException();
	if (localLookup(sym.name()) != null) throw new DuplicateException();
	try {
	    tab = (Hashtable)(myList.getFirst());
	    tab.put(sym.name(), sym);
	} catch (NoSuchElementException ex) {
	    // ne doit ps aller l�
	    System.err.println("unexpected NoSuchElementException in SymTab.insert");
	    System.exit(-1);
	}
    }

    public void addHashtab() {
	myList.addFirst(new Hashtable());
    }

    public void removeHashtab() throws EmptySymTabException {
	try {
	    myList.removeFirst();
	} catch (NoSuchElementException ex) {
	    throw new EmptySymTabException();
	}

    }

    public Sym localLookup(String str) {
	Hashtable tab;

	if (myList.size() == 0) return null;
	try {
	    tab = (Hashtable)(myList.getFirst());
	    return (Sym)(tab.get(str));
	} catch (NoSuchElementException ex) {
	    // de doit pas aller l�
	    System.err.println("unexpected NoSuchElementException in SymTab.insert");
	    System.exit(-1);
	    return null;
	}
    }

    public Sym globalLookup(String str) {
	Hashtable tab;
	Sym tmp;
	Iterator it = myList.iterator();
	try {
	    while (it.hasNext()) {
		tab = (Hashtable)(it.next());
		tmp = (Sym)(tab.get(str));
		if (tmp != null) return tmp;
	    }
	} catch (NoSuchElementException ex) {
	    // ne doit pas aller l�
	    System.err.println("unexpected NoSuchElementException in SymTab.globalLookup");
	    System.exit(-1);
	}
	return null;
    }

    public void print(PrintWriter p) {
	Hashtable tab;
	Iterator it = myList.iterator();

	try {
	    while (it.hasNext()) {
		tab = (Hashtable)(it.next());
		p.println(tab.toString());
	    }
	} catch (NoSuchElementException ex) {
	    // ne pas aller l�
	    System.err.println("unexpected NoSuchElementException in SymTab.print");
	    System.exit(-1);
	}
    }

}

