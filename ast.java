import java.io.*;
import java.util.*;


// The ASTnode class defines the nodes of the abstract-syntax tree that
// represents a C-- program.
//
// Internal nodes of the tree contain pointers to children, organized
// either in a sequence (for nodes that may have a variable number of children)
// or as a fixed set of fields.
//
// The nodes for literals and ids contain line and character number
// information; for string literals and identifiers, they also contain a
// string; for integer literals, they also contain an integer value.
//
// Here are all the different kinds of AST nodes and what kinds of children
// they have.  All of these kinds of AST nodes are Sous classes de "ASTnode".
// Indentation indicates further subclassing:

/* La classe ASTnode d�f�nit les noeuds de l'arbre abtrait qui repr�sente notre
 * programme C
 * 
 * Les noeuds internes d l'arbre contiennent des pointeurs vers les enfants
 * 
 * Les noeuds pour les "literals" et ID's continnent la ligne et les n� de cacact�re
 * Pour les string literals ils contiennent un plus un string
 * Pour les literals de type int, ils contiennent un int en plus
 *
 */

 // Classe ASTnode, classe de base pour tous les noeuds
abstract class ASTnode { 
    // toutes les sous classes doivent avoir la m�thode unparse
    abstract public void unparse(PrintWriter p, int indent);

    // cette m�thode peut �tre utilis�e par la m�thode unparse pour faire l'indentation
    protected void doIndent(PrintWriter p, int indent) {
	for (int k=0; k<indent; k++) p.print(" ");
    }
}


// ProgramNode,  DeclListNode, FormalsListNode, FnBodyNode,
// StmtListNode, ExpListNode

class ProgramNode extends ASTnode {
    public ProgramNode(DeclListNode L) {
	myDeclList = L;
    }

    /* processNames
     *
     * cr�e une table des symboles vide et parcours toutes 
	 * les variables globales et fonctions du programme
     */
    public void processNames() {
	SymTab S = new SymTab();
	myDeclList.processNames(S);
    }

    /* typeCheck */
    public void typeCheck() {
	myDeclList.typeCheck();
    }

    public void unparse(PrintWriter p, int indent) {
	myDeclList.unparse(p, indent);
    }

    // 1 enfant
    private DeclListNode myDeclList;
}

class DeclListNode extends ASTnode {
    public DeclListNode(LinkedList L) {
	myDecls = L;
    }

    /* processNames
     *
     * entr�e: une table des symboles S
     * action: parcours tous les decls de la liste
     */
    public void processNames(SymTab S) {
	Iterator it = myDecls.iterator();
	try {
	    while (it.hasNext()) {
		((DeclNode)it.next()).processNames(S);
	    }
	} catch (NoSuchElementException ex) {
	    System.err.println("unexpected NoSuchElementException in DeclListNode.processNames");
	    System.exit(-1);
	}
	
    }

    /* typeCheck */
    public void typeCheck() {
	Iterator it = myDecls.iterator();
	try {
	    while (it.hasNext()) {
		((DeclNode)it.next()).typeCheck();
	    }
	} catch (NoSuchElementException ex) {
	    System.err.println("unexpected NoSuchElementException in DeclListNode.typeCheck");
	    System.exit(-1);
	}
	
    }

    // ** unparse **
    public void unparse(PrintWriter p, int indent) {
	Iterator it = myDecls.iterator();
	try {
	    while (it.hasNext()) {
		((DeclNode)it.next()).unparse(p, indent);
	    }
	} catch (NoSuchElementException ex) {
	    System.err.println("unexpected NoSuchElementException in DeclListNode.unparse");
	    System.exit(-1);
	}
    }

    // parcours des enfants (DeclNodes)
    private LinkedList myDecls;
}

class FormalsListNode extends ASTnode {
    public FormalsListNode(LinkedList L) {
	myFormals = L;
    }

    /* processNames
     *
     * entr�e: une table de ssymboles S
     * action: parcours touts les formals dans la liste
     */
    public LinkedList processNames(SymTab S) {
	LinkedList L = new LinkedList();
	Iterator it = myFormals.iterator();
	try {
	    while (it.hasNext()) {
		Sym sym = ((FormalDeclNode)it.next()).processNames(S);
		if (sym != null) L.add(sym);
	    }
	} catch (NoSuchElementException ex) {
	    System.err.println("unexpected NoSuchElementException in FormalsListNode.processNames");
	    System.exit(-1);
	}
	return L;
    }

    /* length */
    public int length() {
	return myFormals.size();
    }

    // ** unparse **
    public void unparse(PrintWriter p, int indent) {
	Iterator it = myFormals.iterator();
	try {
	    while (it.hasNext()) {
		((FormalDeclNode)it.next()).unparse(p, indent);
		if (it.hasNext()) {
		    p.print(", ");
		}
	    }
	} catch (NoSuchElementException ex) {
	    System.err.println("unexpected NoSuchElementException in FormalsListNode.unparse");
	    System.exit(-1);
	}
    }

    // parcours des enfants (FormalDeclNodes)
    private LinkedList myFormals;
}

class FnBodyNode extends ASTnode {
    public FnBodyNode(DeclListNode declList, StmtListNode stmtList) {
	myDeclList = declList;
	myStmtList = stmtList;
    }

    /* processNames */
    public void processNames(SymTab S) {
	myDeclList.processNames(S);
	myStmtList.processNames(S);
    }

    /* typeCheck */
    public void typeCheck(Type T) {
	myStmtList.typeCheck(T);
    }

    // ** unparse **
    public void unparse(PrintWriter p, int indent) {
	if (myDeclList != null) myDeclList.unparse(p, indent+2);
	if (myStmtList != null) myStmtList.unparse(p, indent+2);
    }

    // 2 enfants
    private DeclListNode myDeclList;
    private StmtListNode myStmtList;
}

class StmtListNode extends ASTnode {
    public StmtListNode(LinkedList L) {
	myStmts = L;
    }

    /* processNames */
    public void processNames(SymTab S) {
	Iterator it = myStmts.iterator();
	try {
	    while (it.hasNext()) {
		((StmtNode)it.next()).processNames(S);
	    }
	} catch (NoSuchElementException ex) {
	    System.err.println("unexpected NoSuchElementException in StmtListNode.processNames");
	    System.exit(-1);
	}
    }

    /* typeCheck */
    public void typeCheck(Type T) {
	Iterator it = myStmts.iterator();
	try {
	    while (it.hasNext()) {
		((StmtNode)it.next()).typeCheck(T);
	    }
	} catch (NoSuchElementException ex) {
	    System.err.println("unexpected NoSuchElementException in StmtListNode.processNames");
	    System.exit(-1);
	}
    }

    // ** unparse **
    public void unparse(PrintWriter p, int indent) {
	// indent pour chaque stmt (statement)
	// chaque stmt finit par un new line
	Iterator it = myStmts.iterator();
	try {
	    while (it.hasNext()) {
		doIndent(p, indent);
		((StmtNode)it.next()).unparse(p, indent);
	    }
	} catch (NoSuchElementException ex) {
	    System.err.println("unexpected NoSuchElementException in StmtListNode.unparse");
	    System.exit(-1);
	}
    }

    // parcours les enfants (StmtNodes)
    private LinkedList myStmts;
}

class ExpListNode extends ASTnode {
    public ExpListNode(LinkedList L) {
	myExps = L;
    }

    /* typeCheck */
    public void typeCheck(LinkedList L) {
	int k=0;
	Iterator it = myExps.iterator();
	try {
	    while (it.hasNext()) {
		ExpNode exp = (ExpNode)it.next();
		Type actualT = exp.typeCheck();
		if (!actualT.isErrorType()) {
		    Sym sym = (Sym)L.get(k);
		    Type paramT = sym.type();
		    if (!paramT.equals(actualT)) {
			Errors.fatal(exp.linenum(), exp.charnum(),
				     "Ce type est pas formal");
		    }
		}
		k++;
	    }
	} catch (NoSuchElementException ex) {
	    System.err.println("unexpected NoSuchElementException in ExpListNode.processNames");
	    System.exit(-1);
	}
    }

    /* processNames */
    public void processNames(SymTab S) {
	Iterator it = myExps.iterator();
	try {
	    while (it.hasNext()) {
		((ExpNode)it.next()).processNames(S);
	    }
	} catch (NoSuchElementException ex) {
	    System.err.println("unexpected NoSuchElementException in ExpListNode.processNames");
	    System.exit(-1);
	}
    }

    /* length */
    public int length() {
	return myExps.size();
    }

    // ** unparse **
    public void unparse(PrintWriter p, int indent) {
	Iterator it = myExps.iterator();
	try {
	    while (it.hasNext()) {
		((ExpNode)it.next()).unparse(p, 0);
		if (it.hasNext()) {
		    p.print(", ");
		}
	    }
	} catch (NoSuchElementException ex) {
	    System.err.println("unexpected NoSuchElementException in ExpListNode.unparse");
	    System.exit(-1);
	}
    }

    // parcours des enfants (ExpNodes)
    private LinkedList myExps;
}


// DeclNode et ses sous classes
abstract class DeclNode extends ASTnode {

    abstract public Sym processNames(SymTab S);

    // version par d�feut de typeCheck pour decls var et formal
    public void typeCheck() {
    }
}

class VarDeclNode extends DeclNode {
    public VarDeclNode(TypeNode type, IdNode id, int size) {
	myType = type;
	myId = id;
	mySize = size;
    }

    /* processNames */
    public Sym processNames(SymTab S) {
	String name = myId.name();
	boolean badDecl = false;
	if (myType.type(NOT_ARRAY).isVoidType()) {
	    Errors.fatal(myId.linenum(), myId.charnum(),
			 "Non-function declared void");
	    badDecl = true;
	}
	if (S.localLookup(name) != null) {
	    Errors.fatal(myId.linenum(), myId.charnum(),
			 "Multiply declared identifier");
	    badDecl = true;
	}
	if (! badDecl) {
	    try {
		Sym sym = new Sym(name, myType.type(mySize));
		S.insert(sym);
		myId.link(sym);
	    } catch (DuplicateException ex) {
		System.err.println("unexpected DuplicateException in VarDeclNode.processNames");
		System.exit(-1);
	    } catch (EmptySymTabException ex) {
		System.err.println("unexpected EmptySymTabException in VarDeclNode.processNames");
		System.exit(-1);
	    }
	}
	return null;
    }

    // ** unparse **
    public void unparse(PrintWriter p, int indent) {
	doIndent(p, indent);
	myType.unparse(p, 0);
	p.print(" ");
	myId.unparse(p, 0);
	if (mySize != NOT_ARRAY) {
	    p.print("[" + mySize + "]");
	}
	p.println(";");
    }

    // 3 enfants
    private TypeNode myType;
    private IdNode myId;
    private int mySize;

    public static int NOT_ARRAY = -1;
}

class FnDeclNode extends DeclNode {
    public FnDeclNode(TypeNode type,
		      IdNode id,
		      FormalsListNode formalList,
		      FnBodyNode body) {
	myType = type;
	myId = id;
	myFormalsList = formalList;
	myBody = body;
    }

    /* processNames */
    public Sym processNames(SymTab S) {
	String name = myId.name();
	FnSym sym = null;
	if (S.localLookup(name) != null) {
	    Errors.fatal(myId.linenum(), myId.charnum(),
			 "Multiply declared identifier");
	}
	else {
	    try {
		sym = new FnSym(name, myType.type(VarDeclNode.NOT_ARRAY),
				myFormalsList.length());
		S.insert(sym);
		myId.link(sym);
	    } catch (DuplicateException ex) {
		System.err.println("unexpected DuplicateException in FnDeclNode.processNames");
		System.exit(-1);
	    } catch (EmptySymTabException ex) {
		System.err.println("unexpected EmptySymTabException in FnDeclNode.processNames");
		System.exit(-1);
	    }
	}
	S.addHashtab();
	LinkedList L = myFormalsList.processNames(S);
	if (sym != null) sym.addFormals(L);
	myBody.processNames(S);
	try {
	    S.removeHashtab();
	} catch (EmptySymTabException ex) {
		System.err.println("unexpected EmptySymTabException in FnDeclNode.processNames");
		System.exit(-1);
	    }
	return null;
    }

    /* typeCheck */
    public void typeCheck() {
	myBody.typeCheck(myType.type(VarDeclNode.NOT_ARRAY));
    }

    // ** unparse **
    public void unparse(PrintWriter p, int indent) {
	p.println();
	doIndent(p, indent);
	myType.unparse(p, 0);
	p.print(" ");
	myId.unparse(p, 0);
	p.print("(");
	if (myFormalsList != null) myFormalsList.unparse(p, 0);
	p.println(") {");
	if (myBody != null) myBody.unparse(p, indent);
	doIndent(p, indent);
	p.println("}");
    }

    // 4 enfants
    private TypeNode myType; // return type
    private IdNode myId;
    private FormalsListNode myFormalsList;
    private FnBodyNode myBody;
}

class FormalDeclNode extends DeclNode {
    public FormalDeclNode(TypeNode type, IdNode id) {
	myType = type;
	myId = id;
    }

    /* processNames */
    public Sym processNames(SymTab S) {
	String name = myId.name();
	boolean badDecl = false;
	Sym sym = null;
	if (myType.type(VarDeclNode.NOT_ARRAY).isVoidType()) {
	    Errors.fatal(myId.linenum(), myId.charnum(),
			 "Non-function declared void");
	    badDecl = true;
	}
	if (S.localLookup(name) != null) {
	    Errors.fatal(myId.linenum(), myId.charnum(),
			 "Multiply declared identifier");
	    badDecl = true;
	}
	if (! badDecl) {
	    try {
		sym = new Sym(name, myType.type(VarDeclNode.NOT_ARRAY));
		S.insert(sym);
		myId.link(sym);
	    } catch (DuplicateException ex) {
		System.err.println("unexpected DuplicateException in FormalDeclNode.processNames");
		System.exit(-1);
	    } catch (EmptySymTabException ex) {
		System.err.println("unexpected EmptySymTabException in FormalDeclNode.processNames");
		System.exit(-1);
	    }
	}
	return sym;
    }

    // ** unparse **
    public void unparse(PrintWriter p, int indent) {
	doIndent(p, indent);
	myType.unparse(p, indent);
	p.print(" ");
	myId.unparse(p, indent);
    }

    // 2 enfants
    private TypeNode myType;
    private IdNode myId;
}


// TypeNode et ses sous classes

abstract class TypeNode extends ASTnode {
    /* toutes les sous classe ont une m�thode type */
    abstract public Type type(int size);
}

class IntNode extends TypeNode {
    public IntNode() {
    }

    /* type */
    public Type type(int size) {
	if (size == VarDeclNode.NOT_ARRAY) return new IntType();
	else return new IntArrayType(size);
    }

    // ** unparse **
    public void unparse(PrintWriter p, int indent) {
	p.print("int");
    }
}

class BoolNode extends TypeNode {
    public BoolNode() {
    }

    /* type */
    public Type type(int size) {
	if (size == VarDeclNode.NOT_ARRAY) return new BoolType();
	else return new BoolArrayType(size);
    }

    // ** unparse **
    public void unparse(PrintWriter p, int indent) {
	p.print("bool");
    }
}

class VoidNode extends TypeNode {
    public VoidNode() {
    }

    public Type type(int size) {
	return new VoidType();
    }

    // ** unparse **
    public void unparse(PrintWriter p, int indent) {
	p.print("void");
    }
}


// StmtNode et ses sous classes


abstract class StmtNode extends ASTnode {
    abstract public void processNames(SymTab S);
    abstract public void typeCheck(Type T);
}

class ReadStmtNode extends StmtNode {
    public ReadStmtNode(ExpNode e) {
	myExp = e;
    }

    /* processNames */
    public void processNames(SymTab S) {
	myExp.processNames(S);
    }

    /* typeCheck */
    public void typeCheck(Type retType) {
	Type T = myExp.typeCheck();
	if (T.isFnType()) {
	    Errors.fatal(myExp.linenum(), myExp.charnum(),
			 "Attempt to read a function");
	}
	if (T.isArrayType()) {
	    Errors.fatal(myExp.linenum(), myExp.charnum(),
			 "Attempt to read an array");
	}
    }

    public void unparse(PrintWriter p, int indent) {
	p.print("cin >> ");
	myExp.unparse(p,0);
	p.println(";");
    }

    // 1 enfant (ne peut �tre qu'un IdNode ou un ArrayExpNode)
    private ExpNode myExp;
}

class WriteStmtNode extends StmtNode {
    public WriteStmtNode(ExpNode exp) {
	myExp = exp;
    }

    /* typeCheck */
    public void typeCheck(Type retType) {
	Type T = myExp.typeCheck();
	if (T.isFnType()) {
	    Errors.fatal(myExp.linenum(), myExp.charnum(),
			 "Attempt to write a function");
	}
	if (T.isArrayType()) {
	    Errors.fatal(myExp.linenum(), myExp.charnum(),
			 "Attempt to write an array");
	}
    }

    /* processNames */
    public void processNames(SymTab S) {
	myExp.processNames(S);
    }

    // ** unparse **
    public void unparse(PrintWriter p, int indent) {
	p.print("cout << ");
	myExp.unparse(p,0);
	p.println(";");
    }

    // 1 enfant
    private ExpNode myExp;
}

class AssignStmtNode extends StmtNode {
    public AssignStmtNode(ExpNode lhs, ExpNode exp) {
	myLhs = lhs;
	myExp = exp;
    }

    /* typeCheck */
    public void typeCheck(Type retType) {
	Type T1 = myLhs.typeCheck();
	Type T2 = myExp.typeCheck();
	if (T1.isFnType() && T2.isFnType()) {
	    Errors.fatal(myLhs.linenum(), myLhs.charnum(),
			 "Function assignment");
	}
	if (T1.isArrayType() && T2.isArrayType()) {
	    Errors.fatal(myLhs.linenum(), myLhs.charnum(),
			 "Array assignment");
	}
	if (! T1.equals(T2) && ! T1.isErrorType() && ! T2.isErrorType()) {
	    Errors.fatal(myLhs.linenum(), myLhs.charnum(),
			 "Type mismatch");
	}
    }

    /* processNames */
    public void processNames(SymTab S) {
	myLhs.processNames(S);
	myExp.processNames(S);
    }

    // ** unparse **
    public void unparse(PrintWriter p, int indent) {
	myLhs.unparse(p, 0);
	p.print(" = ");
	myExp.unparse(p,0);
	p.println(";");
    }

    // 2 enfant
    private ExpNode myLhs;
    private ExpNode myExp;
}

class IfStmtNode extends StmtNode {
    public IfStmtNode(ExpNode exp, DeclListNode dlist, StmtListNode slist) {
	myDeclList = dlist;
	myExp = exp;
	myStmtList = slist;
    }

    /* typeCheck */
    public void typeCheck(Type retType) {
	Type T = myExp.typeCheck();
	if (! T.isBoolType() && ! T.isErrorType()) {
	    Errors.fatal(myExp.linenum(), myExp.charnum(),
			 "Non-bool expression used as an if condition");
	}
	myStmtList.typeCheck(retType);
    }

    /* processNames */
    public void processNames(SymTab S) {
	myExp.processNames(S);
	S.addHashtab();
	myDeclList.processNames(S);
	myStmtList.processNames(S);
	try {
	    S.removeHashtab();
	} catch (EmptySymTabException ex) {
		System.err.println("unexpected EmptySymTabException in IfStmtNode.processNames");
		System.exit(-1);
	    }
    }

    // ** unparse **
    public void unparse(PrintWriter p, int indent) {
	p.print("if (");
	myExp.unparse(p,0);
	p.println(") {");
	if (myDeclList != null) myDeclList.unparse(p,indent+2);
	if (myStmtList != null) myStmtList.unparse(p,indent+2);
	doIndent(p, indent);
	p.println("}");
    }

    // 3 enfants
    private ExpNode myExp;
    private DeclListNode myDeclList;
    private StmtListNode myStmtList;
}

class IfElseStmtNode extends StmtNode {
    public IfElseStmtNode(ExpNode exp, DeclListNode dlist1,
			  StmtListNode slist1, DeclListNode dlist2,
			  StmtListNode slist2) {
	myExp = exp;
	myThenDeclList = dlist1;
	myThenStmtList = slist1;
	myElseDeclList = dlist2;
	myElseStmtList = slist2;
    }

    /* typeCheck */
    public void typeCheck(Type retType) {
	Type T = myExp.typeCheck();
	if (! T.isBoolType() && ! T.isErrorType()) {
	    Errors.fatal(myExp.linenum(), myExp.charnum(),
			 "Non-bool expression used as an if condition");
	}
	myThenStmtList.typeCheck(retType);
	myElseStmtList.typeCheck(retType);
    }

    /* processNames */
    public void processNames(SymTab S) {
	myExp.processNames(S);
	S.addHashtab();
	myThenDeclList.processNames(S);
	myThenStmtList.processNames(S);
	try {
	    S.removeHashtab();
	} catch (EmptySymTabException ex) {
		System.err.println("unexpected EmptySymTabException in IfElseStmtNode.processNames");
		System.exit(-1);
	    }
	S.addHashtab();
	myElseDeclList.processNames(S);
	myElseStmtList.processNames(S);
	try {
	    S.removeHashtab();
	} catch (EmptySymTabException ex) {
		System.err.println("unexpected EmptySymTabException in IfElseStmtNode.processNames");
		System.exit(-1);
	    }
    }

    // ** unparse **
    public void unparse(PrintWriter p, int indent) {
	p.print("if (");
	myExp.unparse(p,0);
	p.println(") {");
	if (myThenDeclList != null) myThenDeclList.unparse(p,indent+2);
	if (myThenStmtList != null) myThenStmtList.unparse(p,indent+2);
	doIndent(p, indent);
	p.println("}");
	doIndent(p, indent);
	p.println("else {");
	if (myElseDeclList != null) myElseDeclList.unparse(p,indent+2);
	if (myElseStmtList != null) myElseStmtList.unparse(p,indent+2);
	doIndent(p, indent);
	p.println("}");
    }

    // 5 enfants
    private ExpNode myExp;
    private DeclListNode myThenDeclList;
    private StmtListNode myThenStmtList;
    private StmtListNode myElseStmtList;
    private DeclListNode myElseDeclList;
}

class WhileStmtNode extends StmtNode {
    public WhileStmtNode(ExpNode exp, DeclListNode dlist, StmtListNode slist) {
	myExp = exp;
	myDeclList = dlist;
	myStmtList = slist;
    }

    /* typeCheck */
    public void typeCheck(Type retType) {
	Type T = myExp.typeCheck();
	if (! T.isBoolType() && ! T.isErrorType()) {
	    Errors.fatal(myExp.linenum(), myExp.charnum(),
			 "Non-bool expression used as a while condition");
	}
	myStmtList.typeCheck(retType);
    }

    /* processNames */
    public void processNames(SymTab S) {
	myExp.processNames(S);
	S.addHashtab();
	myDeclList.processNames(S);
	myStmtList.processNames(S);
	try {
	    S.removeHashtab();
	} catch (EmptySymTabException ex) {
		System.err.println("unexpected EmptySymTabException in WhileStmtNode.processNames");
		System.exit(-1);
	    }
    }

    // ** unparse **
    public void unparse(PrintWriter p, int indent) {
	p.print("while (");
	myExp.unparse(p,0);
	p.println(") {");
	if (myDeclList != null) myDeclList.unparse(p,indent+2);
	if (myStmtList != null) myStmtList.unparse(p,indent+2);
	doIndent(p, indent);
	p.println("}");
    }

    // 3 enfants
    private ExpNode myExp;
    private DeclListNode myDeclList;
    private StmtListNode myStmtList;
}

class CallStmtNode extends StmtNode {
    public CallStmtNode(CallExpNode call) {
	myCall = call;
    }

    /* typeCheck */
    public void typeCheck(Type retType) {
	myCall.typeCheck();
    }

    /* processNames */
    public void processNames(SymTab S) {
	myCall.processNames(S);
    }

    // ** unparse **
    public void unparse(PrintWriter p, int indent) {
	myCall.unparse(p,indent);
	p.print(";\n");
    }

    // 1 enfant
    private CallExpNode myCall;
}

class ReturnStmtNode extends StmtNode {
    public ReturnStmtNode(ExpNode exp) {
	myExp = exp;
    }

    /* typeCheck */
    public void typeCheck(Type retType) {
	if (myExp != null) {
	    Type T = myExp.typeCheck();
	    if (retType.isVoidType()) {
		Errors.fatal(myExp.linenum(), myExp.charnum(),
			     "Return with a value in a void function");
	    }
	    else if (! T.isErrorType() && ! retType.equals(T)) {
		Errors.fatal(myExp.linenum(), myExp.charnum(),
			     "Bad return value");
	    }
	}
	else {
	    if (! retType.isVoidType()) {
		Errors.fatal(0, 0, "Missing return value");
	    }
	}
    }

    /* processNames */
    public void processNames(SymTab S) {
	if (myExp != null) myExp.processNames(S);
    }

    // ** unparse **
    public void unparse(PrintWriter p, int indent) {
	p.print("return");
	if (myExp != null) {
	    p.print(" ");
	    myExp.unparse(p,0);
	}
	p.print(";\n");
    }

    // 1 enfant
    private ExpNode myExp;
}


// ExpNode et ses sous classes


abstract class ExpNode extends ASTnode {
    public void processNames(SymTab S) {}

    abstract public Type typeCheck();
    abstract public int linenum();
    abstract public int charnum();
}

class IntLitNode extends ExpNode {
    public IntLitNode(int lineNum, int charNum, int intVal) {
	myLineNum = lineNum;
	myCharNum = charNum;
	myIntVal = intVal;
    }

    /* typeCheck */
    public Type typeCheck() {
	return new IntType();
    }

    // ** unparse **
    public void unparse(PrintWriter p, int indent) {
	p.print(myIntVal);
    }

    /* linenum */
    public int linenum() {
	return myLineNum;
    }

    /* charnum */
    public int charnum() {
	return myCharNum;
    }

    private int myLineNum;
    private int myCharNum;
    private int myIntVal;
}

class StringLitNode extends ExpNode {
    public StringLitNode(int lineNum, int charNum, String strVal) {
	myLineNum = lineNum;
	myCharNum = charNum;
	myStrVal = strVal;
    }

    /* typeCheck */
    public Type typeCheck() {
	return new StringType();
    }

    // ** unparse **
    public void unparse(PrintWriter p, int indent) {
	p.print(myStrVal);
    }

    /* linenum */
    public int linenum() {
	return myLineNum;
    }

    /* charnum */
    public int charnum() {
	return myCharNum;
    }

    private int myLineNum;
    private int myCharNum;
    private String myStrVal;
}

class TrueNode extends ExpNode {
    public TrueNode(int lineNum, int charNum) {
	myLineNum = lineNum;
	myCharNum = charNum;
    }

    /* typeCheck */
    public Type typeCheck() {
	return new BoolType();
    }

    // ** unparse **
    public void unparse(PrintWriter p, int indent) {
	p.print("true");
    }

    /* linenum */
    public int linenum() {
	return myLineNum;
    }

    /* charnum */
    public int charnum() {
	return myCharNum;
    }

    private int myLineNum;
    private int myCharNum;
}

class FalseNode extends ExpNode {
    public FalseNode(int lineNum, int charNum) {
	myLineNum = lineNum;
	myCharNum = charNum;
    }

    /* typeCheck */
    public Type typeCheck() {
	return new BoolType();
    }

    // ** unparse **
    public void unparse(PrintWriter p, int indent) {
	p.print("false");
    }

    /* linenum */
    public int linenum() {
	return myLineNum;
    }

    /* charnum */
    public int charnum() {
	return myCharNum;
    }

    // 2 fields
    private int myLineNum;
    private int myCharNum;
}

class IdNode extends ExpNode {
    public IdNode(int lineNum, int charNum, String strVal) {
	myLineNum = lineNum;
	myCharNum = charNum;
	myStrVal = strVal;
    }

    /* typeCheck */
    public Type typeCheck() {
	if (mySym != null) return mySym.type();
	else {
	    System.err.println("ID with null sym field in IdNode.typeCheck");
	    System.exit(-1);
	}
	return null;
    }

    /* processNames */
    public void processNames(SymTab S) {
	Sym sym = S.globalLookup(myStrVal);
	if (sym  == null) {
	    Errors.fatal(myLineNum, myCharNum,
			 "Undeclared identifier");
	}
	else link(sym);
    }

    /* link */
    public void link(Sym sym) {
	mySym = sym;
    }

    // ** unparse **
    public void unparse(PrintWriter p, int indent) {
	p.print(myStrVal);
	if (mySym != null) {
	    p.print("(" + mySym.type() + ")");
	}
    }

    /* name */
    public String name() {
	return myStrVal;
    }

    /* type */
    public Type type() {
	if (mySym != null) return mySym.type();
	else {
	    System.err.println("ID with null sym field");
	    System.exit(-1);
	}
	return null;
    }

    /* symbol-table entry */
    public Sym sym() {
	return mySym;
    }

    /* line num */
    public int linenum() {
	return myLineNum;
    }

    /* char num */
    public int charnum() {
	return myCharNum;
    }

    // fields
    private int myLineNum;
    private int myCharNum;
    private String myStrVal;
    private Sym mySym;
}

class ArrayExpNode extends ExpNode {
    public ArrayExpNode(IdNode id, ExpNode exp) {
	myId = id;
	myExp = exp;
    }

    /* typeCheck */
    public Type typeCheck() {
	Type T = myId.type();
	if (! T.isArrayType()) {
	    Errors.fatal(myId.linenum(), myId.charnum(),
			 "Index applied to non-array operand");
	}
	Type expT = myExp.typeCheck();
	if (! expT.isIntType() && ! expT.isErrorType()) {
	    Errors.fatal(myExp.linenum(), myExp.charnum(),
			 "Non-int expression used as an array index");
	}
	if (T.isArrayType()) return ((ArrayType)T).arrayToScalar();
	else return new ErrorType();
    }

    /* processNames */
    public void processNames(SymTab S) {
	myId.processNames(S);
	myExp.processNames(S);
    }

    // ** unparse **
    public void unparse(PrintWriter p, int indent) {
	myId.unparse(p, 0);
	if (myExp != null) {
	    p.print("[");
	    myExp.unparse(p,0);
	    p.print("]");
	}
    }

    /* linenum */
    public int linenum() {
	return myId.linenum();
    }

    /* charnum */
    public int charnum() {
	return myId.charnum();
    }

    // 2 enfants
    private IdNode myId;
    private ExpNode myExp;
}

class CallExpNode extends ExpNode {
    public CallExpNode(IdNode name, ExpListNode elist) {
	myId = name;
	myExpList = elist;
    }

    public CallExpNode(IdNode name) {
	myId = name;
	myExpList = new ExpListNode(new LinkedList());
    }

    /* typeCheck */
    public Type typeCheck() {
	Type T = myId.typeCheck();
	// check that ID is a fn
	if (! T.isFnType()) {
	    Errors.fatal(myId.linenum(), myId.charnum(),
			 "Attempt to call a non-function");
	    return new ErrorType();
	}

	// check le nombre d'args
	FnSym s = (FnSym)myId.sym();
	if (s == null) {
	    System.out.println("null sym for ID in CallExpNode.typeCheck");
	    System.exit(-1);
	}

	int numParams = s.numparams();
	if (numParams != myExpList.length()) {
	    Errors.fatal(myId.linenum(), myId.charnum(),
			 "Function call with wrong number of args");
	    return s.returnType();
	}

	// check le type des arg
	myExpList.typeCheck(s.paramTypes());
	return s.returnType();
    }

    /* processNames */
    public void processNames(SymTab S) {
	myId.processNames(S);
	myExpList.processNames(S);
    }

    // ** unparse **
    public void unparse(PrintWriter p, int indent) {
	myId.unparse(p,0);
	p.print("(");
	if (myExpList != null) myExpList.unparse(p,0);
	p.print(")");
    }

    /* linenum */
    public int linenum() {
	return myId.linenum();
    }

    /* charnum */
    public int charnum() {
	return myId.charnum();
    }

    // 2 enfants
    private IdNode myId;
    private ExpListNode myExpList;
}

abstract class UnaryExpNode extends ExpNode {
    public UnaryExpNode(ExpNode exp) {
	myExp = exp;
    }

    /* processNames */
    public void processNames(SymTab S) {
	myExp.processNames(S);
    }

    /* linenum */
    public int linenum() {
	return myExp.linenum();
    }

    /* charnum */
    public int charnum() {
	return myExp.charnum();
    }

    // one kid
    protected ExpNode myExp;
}

abstract class BinaryExpNode extends ExpNode {
    public BinaryExpNode(ExpNode exp1, ExpNode exp2) {
	myExp1 = exp1;
	myExp2 = exp2;
    }

    /* processNames */
    public void processNames(SymTab S) {
	myExp1.processNames(S);
	myExp2.processNames(S);
    }

    /* linenum */
    public int linenum() {
	return myExp1.linenum();
    }

    /* charnum */
    public int charnum() {
	return myExp1.charnum();
    }

    // twoenfants
    protected ExpNode myExp1;
    protected ExpNode myExp2;
}


// sous classes de UnaryExpNode


class UnaryMinusNode extends UnaryExpNode {
    public UnaryMinusNode(ExpNode exp) {
	super(exp);
    }

    /* typeCheck */
    public Type typeCheck() {
	Type T = myExp.typeCheck();
	if (! T.isIntType() && ! T.isErrorType()) {
	    Errors.fatal(myExp.linenum(), myExp.charnum(),
			 "Arithmetic operator applied to non-numeric operand");
	    return new ErrorType();
	}
	if (T.isErrorType()) return T;
	else return new IntType();
    }

    // ** unparse **
    public void unparse(PrintWriter p, int indent) {
	p.print("(-");
	myExp.unparse(p, 0);
	p.print(")");
    }
}

class NotNode extends UnaryExpNode {
    public NotNode(ExpNode exp) {
	super(exp);
    }

    /* typeCheck */
    public Type typeCheck() {
	Type T = myExp.typeCheck();
	if (! T.isBoolType() && ! T.isErrorType()) {
	    Errors.fatal(myExp.linenum(), myExp.charnum(),
			 "Logical operator applied to non-bool operand");
	    return new ErrorType();
	}
	if (T.isErrorType()) return T;
	else return new BoolType();
    }

    // ** unparse **
    public void unparse(PrintWriter p, int indent) {
	p.print("(!");
	myExp.unparse(p, 0);
	p.print(")");
    }
}


// sous classes de BinaryExpNode


abstract class ArithmeticExpNode extends BinaryExpNode {
    public ArithmeticExpNode(ExpNode exp1, ExpNode exp2) {
	super(exp1, exp2);
    }

    /* typeCheck */
    public Type typeCheck() {
	Type T1 = myExp1.typeCheck();
	Type T2 = myExp2.typeCheck();
	Type retType = new IntType();
	if (! T1.isIntType() && ! T1.isErrorType()) {
	    Errors.fatal(myExp1.linenum(), myExp1.charnum(),
			 "Arithmetic operator applied to non-numeric operand");
	    retType = new ErrorType();
	}
	if (! T2.isIntType() && ! T2.isErrorType()) {
	    Errors.fatal(myExp2.linenum(), myExp2.charnum(),
			 "Arithmetic operator applied to non-numeric operand");
	    retType = new ErrorType();
	}
	if (T1.isErrorType() || T2.isErrorType()) return new ErrorType();
	else return retType;
    }
}

abstract class LogicalExpNode extends BinaryExpNode {
    public LogicalExpNode(ExpNode exp1, ExpNode exp2) {
	super(exp1, exp2);
    }

    /* typeCheck */
    public Type typeCheck() {
	Type T1 = myExp1.typeCheck();
	Type T2 = myExp2.typeCheck();
	Type retType = new BoolType();
	if (! T1.isBoolType() && ! T1.isErrorType()) {
	    Errors.fatal(myExp1.linenum(), myExp1.charnum(),
			 "Logical operator applied to non-bool operand");
	    retType = new ErrorType();
	}
	if (! T2.isBoolType() && ! T2.isErrorType()) {
	    Errors.fatal(myExp2.linenum(), myExp2.charnum(),
			 "Logical operator applied to non-bool operand");
	    retType = new ErrorType();
	}
	if (T1.isErrorType() || T2.isErrorType()) return new ErrorType();
	else return retType;
    }
}

abstract class EqualityExpNode extends BinaryExpNode {
    public EqualityExpNode(ExpNode exp1, ExpNode exp2) {
	super(exp1, exp2);
    }

    /* typeCheck */
    public Type typeCheck() {
	Type T1 = myExp1.typeCheck();
	Type T2 = myExp2.typeCheck();
	Type retType = new BoolType();
	if (T1.isArrayType() && T2.isArrayType()) {
	    Errors.fatal(myExp1.linenum(), myExp1.charnum(),
			 "Equality operator applied to arrays");
	    retType = new ErrorType();
	}
	if (T1.isFnType() && T2.isFnType()) {
	    Errors.fatal(myExp1.linenum(), myExp1.charnum(),
			 "Equality operator applied to functions");
	    retType = new ErrorType();
	}
	if (! T1.equals(T2) && ! T1.isErrorType() && ! T2.isErrorType()) {
	    Errors.fatal(myExp1.linenum(), myExp1.charnum(),
			 "Type mismatch");
	    retType = new ErrorType();
	}
	if (T1.isErrorType() || T2.isErrorType()) return new ErrorType();
	else return retType;
    }
}

abstract class RelationalExpNode extends BinaryExpNode {
    public RelationalExpNode(ExpNode exp1, ExpNode exp2) {
	super(exp1, exp2);
    }

    /* typeCheck */
    public Type typeCheck() {
	Type T1 = myExp1.typeCheck();
	Type T2 = myExp2.typeCheck();
	Type retType = new BoolType();
	if (! T1.isIntType() && ! T1.isErrorType()) {
	    Errors.fatal(myExp1.linenum(), myExp1.charnum(),
			 "Relational operator applied to non-numeric operand");
	    retType = new ErrorType();
	}
	if (! T2.isIntType() && ! T2.isErrorType()) {
	    Errors.fatal(myExp2.linenum(), myExp2.charnum(),
			 "Relational operator applied to non-numeric operand");
	    retType = new ErrorType();
	}
	if (T1.isErrorType() || T2.isErrorType()) return new ErrorType();
	else return retType;
    }
}


// Sous classes de ArithmeticExpNode


class PlusNode extends ArithmeticExpNode {
    public PlusNode(ExpNode exp1, ExpNode exp2) {
	super(exp1, exp2);
    }

    // ** unparse **
    public void unparse(PrintWriter p, int indent) {
	p.print("(");
	myExp1.unparse(p, 0);
	p.print("+");
	myExp2.unparse(p, 0);
	p.print(")");
    }
}

class MinusNode extends ArithmeticExpNode {
    public MinusNode(ExpNode exp1, ExpNode exp2) {
	super(exp1, exp2);
    }

    // ** unparse **
    public void unparse(PrintWriter p, int indent) {
	p.print("(");
	myExp1.unparse(p, 0);
	p.print("-");
	myExp2.unparse(p, 0);
	p.print(")");
    }
}

class TimesNode extends ArithmeticExpNode {
    public TimesNode(ExpNode exp1, ExpNode exp2) {
	super(exp1, exp2);
    }

    // ** unparse **
    public void unparse(PrintWriter p, int indent) {
	p.print("(");
	myExp1.unparse(p, 0);
	p.print("*");
	myExp2.unparse(p, 0);
	p.print(")");
    }
}

class DivideNode extends ArithmeticExpNode {
    public DivideNode(ExpNode exp1, ExpNode exp2) {
	super(exp1, exp2);
    }

    // ** unparse **
    public void unparse(PrintWriter p, int indent) {
	p.print("(");
	myExp1.unparse(p, 0);
	p.print("/");
	myExp2.unparse(p, 0);
	p.print(")");
    }
}


// Sous classes de LogicalExpNode


class AndNode extends LogicalExpNode {
    public AndNode(ExpNode exp1, ExpNode exp2) {
	super(exp1, exp2);
    }

    // ** unparse **
    public void unparse(PrintWriter p, int indent) {
	p.print("(");
	myExp1.unparse(p, 0);
	p.print("&&");
	myExp2.unparse(p, 0);
	p.print(")");
    }
}

class OrNode extends LogicalExpNode {
    public OrNode(ExpNode exp1, ExpNode exp2) {
	super(exp1, exp2);
    }

    // ** unparse **
    public void unparse(PrintWriter p, int indent) {
	p.print("(");
	myExp1.unparse(p, 0);
	p.print("||");
	myExp2.unparse(p, 0);
	p.print(")");
    }
}


// Sous classes de EqualityExpNode


class EqualsNode extends EqualityExpNode {
    public EqualsNode(ExpNode exp1, ExpNode exp2) {
	super(exp1, exp2);
    }

    // ** unparse **
    public void unparse(PrintWriter p, int indent) {
	p.print("(");
	myExp1.unparse(p, 0);
	p.print("==");
	myExp2.unparse(p, 0);
	p.print(")");
    }
}

class NotEqualsNode extends EqualityExpNode {
    public NotEqualsNode(ExpNode exp1, ExpNode exp2) {
	super(exp1, exp2);
    }

    // ** unparse **
    public void unparse(PrintWriter p, int indent) {
	p.print("(");
	myExp1.unparse(p, 0);
	p.print("!=");
	myExp2.unparse(p, 0);
	p.print(")");
    }
}


// Sous classes de RelationalExpNode


class LessNode extends RelationalExpNode {
    public LessNode(ExpNode exp1, ExpNode exp2) {
	super(exp1, exp2);
    }

    // ** unparse **
    public void unparse(PrintWriter p, int indent) {
	p.print("(");
	myExp1.unparse(p, 0);
	p.print("<");
	myExp2.unparse(p, 0);
	p.print(")");
    }
}

class GreaterNode extends RelationalExpNode {
    public GreaterNode(ExpNode exp1, ExpNode exp2) {
	super(exp1, exp2);
    }

    // ** unparse **
    public void unparse(PrintWriter p, int indent) {
	p.print("(");
	myExp1.unparse(p, 0);
	p.print(">");
	myExp2.unparse(p, 0);
	p.print(")");
    }
}

class LessEqNode extends RelationalExpNode {
    public LessEqNode(ExpNode exp1, ExpNode exp2) {
	super(exp1, exp2);
    }

    // ** unparse **
    public void unparse(PrintWriter p, int indent) {
	p.print("(");
	myExp1.unparse(p, 0);
	p.print("<=");
	myExp2.unparse(p, 0);
	p.print(")");
    }
}

class GreaterEqNode extends RelationalExpNode {
    public GreaterEqNode(ExpNode exp1, ExpNode exp2) {
	super(exp1, exp2);
    }

    // ** unparse **
    public void unparse(PrintWriter p, int indent) {
	p.print("(");
	myExp1.unparse(p, 0);
	p.print(">=");
	myExp2.unparse(p, 0);
	p.print(")");
    }
}

