import java.util.*;

// **********************************************************************
// La classe Sym d�finit un symbole, un objet qui contient un name (String) et une type (Type)
// **********************************************************************


class Sym {
	
	private String myName;
    private Type myType;
	
	// Cr�e un sym avec le nom S
    public Sym(String S) {
	myName = S;
    }
	
	// Cr�e un  Sym de type T, nom S
    public Sym(String S, Type T) {
	myName = S;
	myType = T;
    }
	
    public String name() {
	return myName;
    }

    public Type type() {
	return myType;
    }

    public String toString() {
	return "";
    }
}

// **********************************************************************
// FnSym est une sous classe de Sym, juste pour ajouter des fonctions.
// **********************************************************************
class FnSym extends Sym {

	private Type myReturnType;
    private int myNumParams;
    private LinkedList myParamTypes; 
	
    public FnSym(String S, Type T, int numparams) {
	super(S, new FnType());
	myReturnType = T;
	myNumParams = numparams;
    }

    public void addFormals(LinkedList L) {
	myParamTypes = L;
    }
    
    public Type returnType() {
	return myReturnType;
    }

    public int numparams() {
	return myNumParams;
    }

    public LinkedList paramTypes() {
	return myParamTypes;
    }
}
